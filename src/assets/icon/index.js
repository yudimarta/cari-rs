import IconBackDark from './arrow_back_24px.svg';
import IconBackLight from './ic-back-light.svg';
import IconAddPhoto from './ic-add-photo.svg';
import IconRemovePhoto from './ic-remove-photo.svg';
import IconNext from './ic-next.svg';

export { IconBackDark, IconBackLight, IconAddPhoto, IconRemovePhoto, IconNext };