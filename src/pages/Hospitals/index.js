import React, { useState, useEffect, useRef } from 'react';
import { StyleSheet, Text, View, Dimensions, TextInput, TouchableOpacity, ScrollView, FlatList, Linking } from 'react-native';
import { colors, fonts, showError } from '../../utils';
import { Button, Input, Link, Gap, Header, List, HospitalList } from '../../component';
import Icons from 'react-native-vector-icons/Ionicons';
import {Picker} from '@react-native-community/picker';
import axios from 'axios';
import { JSONProvince } from '../../assets';
import { useSelector, useDispatch } from 'react-redux';

export default function Hospitals({navigation, route}) {
    const itemDetail = route.params;
    const [refreshing, setRefreshing] = useState(true);
    const [searched, setSearched] = useState([]);
    const [un, setUn] = useState("");
    const [word, setWord] = useState('');
    const [listHospitals, setListHospitals] = useState([]);

    useEffect(() => {
        getHospitals();
        // console.log(itemDetail.value)
        setRefreshing(true)
    }, []);

    const startNavigation = (url) => {
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
              Linking.openURL(url);
            } else {
              console.log('Don\'t know how to open URI: ' + url);
            }
          });
    }

    const startCalling = (number) => {
        Linking.canOpenURL(`tel:${number}`).then(supported => {
            if (supported) {
              Linking.openURL(`tel:${number}`);
            } else {
              console.log('Don\'t know how to call: ' + number);
            }
          });
    }

    const getHospitals = async() => {
        try {
            let response = await axios.get(`https://ina-covid-bed.vercel.app/api/bed?prov=${itemDetail.value}`);
            if(response.status !== 400){
                setListHospitals(response.data.data);
                setSearched(response.data.data);
                if(response.data.data.length === 0){
                    setUn(`Maaf, Tidak ada Kamar tersedia \ndi Provinsi ${itemDetail.name}`);
                }
                setRefreshing(false);
            }
            console.log(response.data.data);
        } catch (error) {
            showError(error.message);
        }
    }

    const filterByValue = (value) => {
        return searched.filter((data) =>  JSON.stringify(data).toLowerCase().indexOf(value.toLowerCase()) !== -1)
    }

    const renderItemHospitals = ({ item }) => {
        return (
            <HospitalList
                name={item.name}
                address={item.address}
                minutes={item.updated_at_minutes}
                phone={item.hotline}
                available={item.available_bed}
                queue={item.bed_queue}
                OnPress={() => {
                    navigation.navigate('WebPage', {url : `${item.bed_detail_link}`})
                }}
                OnMap={() => {
                    startNavigation(`http://maps.google.com/maps?daddr=${item.lat},${item.lon}`)
                }}
                OnDetail={() => {
                    navigation.navigate('WebPage', {url : `${item.bed_detail_link}`})
                }}
                OnCall={() => {
                    startCalling(item.hotline);
                }}
            />
        );
    };

    return (
        <View style={styles.page}>
            <Header type="dark" title={`Daftar Rumah Sakit di \n${itemDetail.name}`} onPress={() => navigation.goBack()}/>
            <View style={styles.searchBar}>
                <View style={styles.searchBarInside}>
                    {word ? 
                    <TouchableOpacity onPress={() => {
                            setWord('')
                            setListHospitals(filterByValue(''))
                        }}>
                        <Icons name="close-outline" size={25} color="#979797" style={{ marginLeft: 13, marginTop: 17 }}/> 
                    </TouchableOpacity> :
                    <Icons name="search" size={20} color="#979797" style={{ marginLeft: 13, marginTop: 17 }}/>
                    }
                    <TextInput
                        placeholder="Cari Rumah Sakit"
                        placeholderTextColor="#8E8E93"
                        style={styles.searchBarTextInput}
                        value={word}
                        autoFocus={true}
                        onChangeText={(val) => {
                            setWord(val)
                            setListHospitals(filterByValue(val))
                        }}
                        onSubmitEditing={()=> {
                            setListHospitals(filterByValue(word))
                        }}
                    />
                </View>
            </View>
            {listHospitals.length ? 
            <Text style={styles.result}>Rumah Sakit Tersedia : {listHospitals.length}</Text>
            :
            <View style={{flex: 1, alignItems:'center', justifyContent: 'center'}}>
                <Text style={styles.unavailable}>{un}</Text>
            </View>
            }
            <FlatList
                data = {listHospitals}
                showsHorizontalScrollIndicator={false}
                renderItem={renderItemHospitals}
                keyExtractor={(item) => item.hospital_code}
                refreshing={refreshing}
                onRefresh={getHospitals}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: colors.white
    },
    searchBar: {
        height: Dimensions.get('window').height * 0.1,
        backgroundColor: '#FFFFFF',
        paddingHorizontal: 24,
        justifyContent: 'center',
        marginTop: 12
      },
      searchBarInside: {
        flexDirection: 'row',
        backgroundColor: '#F3F1F1',
        borderRadius: 8,
        width: '100%'
      },
      searchBarTextInput: {
        height: 56,
        width: 320,
        fontSize: 14,
        lineHeight: 21,
        paddingLeft: 13, fontFamily: fonts.primary[300]
      },
      result: {
          fontSize: 14,
          paddingLeft: 24,
          fontFamily: fonts.primary[300],
          marginBottom: 12,
          color: colors.text.secondary
      },
      unavailable: {
        fontSize: 18,
        fontFamily: fonts.primary[800],
        color: colors.text.primary,
        textAlign: 'center'
    },
})
