import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { IconNext } from '../../../assets'
import { colors, fonts } from '../../../utils'
import Icons from 'react-native-vector-icons/MaterialCommunityIcons'

export default function List({profile, name, desc, type, onPress, icon, solved}) {
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <Icons name="city" color={colors.primary} size={35}/>
            <View style={styles.content}>
                <Text style={styles.name}>{name}</Text>
            </View>
            {type === 'next' && <IconNext />}
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    avatar: {
        width: 46,
        height: 46,
        borderRadius: 46/2
    },
    container: {
       flexDirection: 'row',
       padding: 16,
       borderBottomWidth: 1,
       borderBottomColor: colors.border,
       alignItems: 'center',
       justifyContent: 'space-between'
    },
    name: {
        fontSize: 18,
        fontFamily: fonts.primary[800],
        color: colors.text.primary
    },
    desc: {
        fontSize: 12,
        fontFamily: fonts.primary[300],
        color: colors.text.secondary,
        // textTransform: 'capitalize'
    },
    content: {
        flex: 1,
        marginLeft: 16
    }
})
