import React, { useState, useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import Router from './router';
import { Provider, useSelector } from 'react-redux';
import store from './redux/store';

import codePush from 'react-native-code-push';

let codePushOptions = {
  checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
  installMode: codePush.InstallMode.IMMEDIATE
};


const MainApp = () => {
  return (
      <NavigationContainer>
        <Router />
      </NavigationContainer>
  );
};

const App = () => {
  return (
     <Provider store={store}>
        <MainApp />
     </Provider> 
  )
}

export default App;