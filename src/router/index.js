import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import { Splash, Home, Hospitals, WebPage } from '../pages';

const Stack = createStackNavigator();
  
  const Router = () => {
    return (
      <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen name="Splash" component={ Splash } options={{ headerShown: false }}/>
        <Stack.Screen name="Home" component={ Home } options={{ headerShown: false }}/>
        <Stack.Screen name="Hospitals" component={ Hospitals } options={{ headerShown: false }}/>
        <Stack.Screen name="WebPage" component={ WebPage } options={{ headerShown: false }}/>
      </Stack.Navigator>
    );
  };
  
  export default Router;